var group__ustdlib__api =
[
    [ "ulocaltime", "group__ustdlib__api.html#ga06502f734fc4ece2a831fa0046c528bc", null ],
    [ "usnprintf", "group__ustdlib__api.html#ga0523e2ba8a83e7cfc76be3caaf37b25a", null ],
    [ "usprintf", "group__ustdlib__api.html#ga8d8940f65bf237f422d3a63312942a10", null ],
    [ "ustrcpy", "group__ustdlib__api.html#ga5edba5a9ec1df98e7a4552bda0cddcda", null ],
    [ "ustrstr", "group__ustdlib__api.html#gac2eecc2b64595f203ab61479a7729036", null ],
    [ "ustrtoul", "group__ustdlib__api.html#ga22fa13dc554cf317be1f6910e070b359", null ],
    [ "uvsnprintf", "group__ustdlib__api.html#gad3b713ceeb82516ed0373e7d4343d6e9", null ]
];